class BaseCalculator {
    constructor(init) {
        if (typeof (init) === 'undefined') {
            this._result = 0;
        } else {
            this._result = init;
        }
    }

    sum(...values) {
        for (let i = 0; i < values.length; i++) {
            this._result += values[i];
        }
    }

    dif(...values) {
        for (let i = 0; i < values.length; i++) {
            this._result -= values[i];
        }
    }

    div(...values) {
        for (let i = 0; i < values.length; i++) {
            if (values[i] !== 0) {
                this._result = this._result / values[i];
            } else {
                console.log('на ноль делить нельзя');
            }
        }
    }

    mul(...values) {
        for (let i = 0; i < values.length; i++) {
            this._result = this._result * values[i];
        }

    }

    get result() {
        return this._result;
    }
}

class SqrCalc extends BaseCalculator {
    constructor(init) {
        super(init);
    }

    square() {
        return Math.pow(this._result, 2)
    }

    sum(...values) {
        super.sum(...values);
        return this.square();
    }

    dif(...values) {
        super.dif(...values);
        return this.square();
    }

    div(...values) {
        super.div(...values);
        return this.square();
    }

    mul(...values) {
        super.mul(...values);
        return this.square();
    }
}

let myCalc = new BaseCalculator();
let myCalculator = new SqrCalc(100);

console.log(myCalculator.sum(2));
console.log(myCalculator.dif(10));
console.log(myCalculator.div(2));
console.log(myCalculator.mul(2));
