//сравнение массивов
function isEqualList(a, b) {
    if (a.length !== b.length) {
        return false;
    } else {
        for (let i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) {
                return false;
            }
        }
        return true;
    }
}

//сравнение простых объектов
function isEqualSimpleObject(a, b) {
    //сортируем свойства объектов
    let aKeys = Object.keys(a).sort();
    let bKeys = Object.keys(b).sort();

    if (aKeys.length !== bKeys.length) {
        return false;
    } else {
        // сравнение ключей
        for (let i = 0; i < aKeys.length; i++) {
            if (aKeys[i] !== bKeys[i]) {
                return false;
            }
        }
        //сравнение значений
        for (let i = 0; i < aKeys.length; i++) {
            if (a[aKeys[i]] !== b[bKeys[i]]) {
                return false;
            }
        }
        return true;
    }
}

function isEqualDeep(a, b) {
    let aKeys = Object.keys(a).sort();
    let bKeys = Object.keys(b).sort();
    if (aKeys.length !== bKeys.length) {
        return false;
    } else {
        for (let i = 0; i < aKeys.length; i++) {
            if (aKeys[i] !== bKeys[i]) {
                return false;
            }
        }
        keys = aKeys;
        for (let i = 0; i < aKeys.length; i++) {
            //сравниваем типы каждого значения
            if (typeof (a[keys[i]]) !== typeof (b[keys[i]])) {
                return false;
            } else {
                //если тип - массив, вызываем функцию isEqualList, которая сравнивает массивы
                if (Array.isArray(a[keys[i]]) && Array.isArray(b[keys[i]])) {
                    if (isEqualList(a[keys[i]], b[keys[i]]) === false) {
                        return false;
                    }
                    //если тип - объект, вызываем функцию isEqualSimpleObject, которая сравнивает объекты
                } else if (typeof (a[keys[i]]) == "object" && typeof (b[keys[i]]) == "object") {
                    if (isEqualSimpleObject(a[keys[i]], b[keys[i]]) === false) {
                        return false;
                    }
                } else {
                    if (a[keys[i]] !== b[keys[i]]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


module.exports = isEqualDeep;