function mySplice(arr, start, deleteCount, ...items) {
    if (start < 0) {
        if (Math.abs(start) <= arr.length) {
            start = arr.length - Math.abs(start);//если start отрицательный, то он указывает на индекс элемента с конца массива
        } else {
            start = 0; //если длина массива меньше чем абсолютное значение start, то принимаем start = 0
        }
    } else if (start > arr.length) {
        start = arr.length;//если start больше, чем длина массива, то реальный индекс будет установлен на длину массива
    }
    //если есть элементы, которые нужно добавить в массив
    if (items.length > 0) {
        let N = start + items.length; // количество итераций
        let i = 0;
        while (start < N) {
            if (deleteCount === 0) {
                // добавляем элемент, если нет элементов, которые нужно удалить
                // добавляем элемент в конец массива и переставляем последний элемент в нужную позицию
                arr[arr.length] = items[i];
                let tmp = items[i];
                for (let i = arr.length - 1; i > start; i--) {
                    arr[i] = arr[i - 1];
                }
                arr[start] = tmp;
            } else {
                // выполняем простую замену, если есть элементы, которые нужно удалить
                if (start < arr.length) {
                    arr[start] = items[i];
                } else {
                    arr[arr.length] = items[i];
                }
                i = i + 1;
                deleteCount = deleteCount - 1;
            }
            start = start + 1;
        }
    }
    //удаление элементов массива, если они остались
    if (deleteCount > 0) {
        if (deleteCount >= arr.length - start) {
            arr.length = start;
        } else {
            for (let i = start; i < arr.length - deleteCount; i++) {
                arr[i] = arr[i + deleteCount];
            }
            arr.length = arr.length - deleteCount;
        }
    }
    return arr;
}

module.exports = mySplice;



