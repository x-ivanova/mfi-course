let allCookies = parseCookies(document.cookie);

insertElement(generateElement(allCookies));

// вставляем все cookie в объект вида {cookie's_name: {name:cookie's_name, value:cookie's_value}, ...}
function parseCookies(strCookie) {
    let allCookies = {};
    if (strCookie !== '') {
        strCookie = strCookie.split(";");
        for (let i = 0; i < strCookie.length; i++) {
            let [name, value] = strCookie[i].split("=");
            name = name.replace(/\s/g, '');
            allCookies[name] = {
                "name": name,
                "value": value
            }

        }
    }
    return allCookies;
}

//генерируем HTML элемент(ы), чтобы потом вставить его(их) в таблицу на странице
function generateElement(parsedData) {
    let insertHTML = '';
    if (Array.isArray(parsedData)){
        for (let i = 0; i < parsedData.length; i++) {
            insertHTML += `<td>${parsedData[i]}</td>`;
        }
        insertHTML += `<td><button class="btn" onclick="deleteCookie('${parsedData[0]}');">Удалить cookie</button></td>`;
    } else {
        for (let key in parsedData) {
            let name = parsedData[key]["name"];
            let value = parsedData[key]["value"];
            let expires = localStorage.getItem(name);
            insertHTML += `<tr id="${name}"><td>${name}</td><td>${value}</td><td>${expires}</td><td><button class="btn" onclick="deleteCookie('${name}');">Удалить cookie</button></td></tr>`;
        }
    }
    return insertHTML;
}

// вставляем сгенерированный элемент в нужное место на странице
function insertElement(str, id = 'tbody', tagName = 'tbody', place = 'table') {
    let newEl = document.createElement(tagName);
    newEl.id = id;
    newEl.innerHTML = str;
    let placeInsert = document.getElementById((place));
    placeInsert.insertAdjacentElement('beforeend', newEl);
}

// получаем введеные значения из формы в массив args, если какое-то поле пустое - выводим alert
function getFormInputs(id) {
    let form = document.getElementById(id);
    let args = [];
    for (let i = 0; i < form.elements.length - 1; i++) {
        if (!form.elements[i].value) {
            alert('Пожалуйста, заполните все поля!');
            return false;
        }
        if (form.elements[2].value === '0') {
            alert('Количество дней должно быть больше 0');
            return false;
        }
        args[i] = form.elements[i].value;
    }
    return args;
}

// используется если пользователь через форму пытается добавить cookie с именем, которое уже есть в cookies.
// функция подменяет значение и кол-во дней существования cookie в таблице на странице; заменяет саму cookie и меняет кол-во дней в local storage
function changeCookie(args) {
    let tr = document.getElementById(args[0]);
    let tdValue = tr.childNodes[1];
    let tdDate = tr.childNodes[2];
    tdValue.innerHTML = args[1];
    tdDate.innerHTML = args[2];
    setCookie(args);
    localStorage.setItem(args[0], args[2]);
}


function cookieAction(id) {
    let args = getFormInputs(id); // получаем введенные значения из формы
    if (args !== false) {
        if (allCookies.hasOwnProperty(args[0])) { // если уже есть cookie с таким названием
            changeCookie(args);
        } else {
            addCookie(args);
        }
    }
}

function addCookie(args) {
    setCookie(args);
    localStorage.setItem(args[0], args[2]);
    allCookies[args[0]] = {name: args[1], value: args[2]};
    insertElement(generateElement(args), args[0], 'tr', 'tbody');
}


function deleteCookie(id) {
    let question = confirm(`Вы точно хотите удалить cookie с именем ${id}?`);
    if (question) {
        let deleteNode = document.getElementById(id);
        deleteNode.remove();
        let args = [id, allCookies[id]["value"], -1];
        setCookie(args);
        delete allCookies[id];
        localStorage.removeItem(id);
    }
}

function setCookie(args) {
    let d = new Date();
    d.setTime(d.getTime() + (args[2] * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = args[0] + "=" + args[1] + ";" + expires + ";path=/";
}
