let sliderData = [
    {
        image: "../images/new-york.jpeg",
        title: "New York",
        href: "https://ru.wikipedia.org/wiki/%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA"
    },
    {
        image: "../images/san-francisco.jpeg",
        title: "San Francisco",
        href: "https://ru.wikipedia.org/wiki/%D0%A1%D0%B0%D0%BD-%D0%A4%D1%80%D0%B0%D0%BD%D1%86%D0%B8%D1%81%D0%BA%D0%BE"
    },
    {
        image: "../images/los-angeles.jpeg",
        title: "Los Angeles",
        href: "https://ru.wikipedia.org/wiki/%D0%9B%D0%BE%D1%81-%D0%90%D0%BD%D0%B4%D0%B6%D0%B5%D0%BB%D0%B5%D1%81"
    },
];

function generateSliderTNS(container) {
    return ({
        "container": container,
        "items": 1,
        "loop": false,
        "mode": "gallery",
        "speed": 1500,
        "nav": false,
        "controlsText": ["<", ">"]
    });
}

function* getOneSlideData(sliderData) {
    for (let i = 0; i < sliderData.length; i++) {
        yield sliderData[i];
    }
}

function createSlideContent(emptySlider, oneSlideData) {
    //create button wrapper
    let slideBtn = document.createElement('div');
    slideBtn.classList.add('btn-more');
    emptySlider.append(slideBtn);

    //insert href
    let slideHref = document.createElement(('a'));
    slideHref.setAttribute('href', oneSlideData.value.href);
    slideHref.setAttribute('target', '_blank');
    slideHref.innerHTML = 'Learn more';
    slideBtn.append(slideHref);

    //add image
    let slideImage = document.createElement('img');
    slideImage.setAttribute('src', oneSlideData.value.image);
    emptySlider.append(slideImage);

    //add text
    let slideTitle = document.createElement('h3');
    slideTitle.innerHTML = oneSlideData.value.title;
    emptySlider.append(slideTitle);

    emptySlider.classList.remove('empty-slide');
}

export { sliderData, generateSliderTNS, getOneSlideData, createSlideContent };
