import '../scss/base.scss';
import {tns} from "../node_modules/tiny-slider/src/tiny-slider";
import {sliderData} from "./slider";
import {generateSliderTNS} from "./slider";
import {getOneSlideData} from "./slider";
import {createSlideContent} from "./slider";

document.addEventListener('DOMContentLoaded', function () {
    let oneSlideData = getOneSlideData(sliderData);
    let slider = tns(generateSliderTNS('.my-slider'));
    let nextButton = slider.getInfo().nextButton;
    let prevButton = slider.getInfo().prevButton;

    nextButton.classList.add('next-button');
    prevButton.classList.add('prev-button');

    function createSlide(oneSlideData) {
        if (!oneSlideData.done) {
            let emptySlide = document.querySelector('.empty-slide');
            createSlideContent(emptySlide, oneSlideData);
        }
    }

    createSlide(oneSlideData.next());
    nextButton.addEventListener('click', function() {
        createSlide(oneSlideData.next());
    });
});
