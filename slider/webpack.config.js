let path = require('path');
let MiniCssExtractPlugin = require('mini-css-extract-plugin')

let conf = {
    entry: './js/index.js',
    output: {
        path: path.resolve(__dirname, './build'), // в идеале полный
        filename: 'bundle.js',
        publicPath: 'build/'
    },
    devServer: {
        overlay: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                //exclude: 'node_modules'
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: { sourceMap: true }
                    },
                    {
                        loader: "sass-loader",
                        options: { sourceMap: true }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "style.css",
            publicPath: 'css/'
        })
    ]

};

module.exports = (env, options) => {
    conf.devtool = options.mode === 'production' ? false : 'cheap-module-eval-source-map';
    return conf;
};
