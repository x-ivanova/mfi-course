import '@babel/polyfill';
import * as ArticlesModel from './articles';

async function articleActions(){
    let articles = await ArticlesModel.all();
    //получаем количество статей
    console.log('articles count = ' + articles.length);
    // берём случайный индекс
    let ind = Math.floor(Math.random() * articles.length);
    console.log('select index ' + ind + ', id = ' + articles[ind].id);
    // получаем статью по id
    let article = await ArticlesModel.one(articles[ind].id);
    console.log(article);
    // пытаемся удалить эту статью
    let res = await ArticlesModel.remove(article.id);
    console.log('что с удалением? - ' + res);
    // снова получаем количество статей
    let newArticles = await ArticlesModel.all();
    console.log('articles count = ' + newArticles.length);
}

articleActions()
    .catch((error) => {
    console.log(error);
});




