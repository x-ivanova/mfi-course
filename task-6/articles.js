import * as serverApi from './db';

async function all() {
    let response = await serverApi.all();
    return parseServerResponse(response);
}

async function one(id) {
    let response = await serverApi.get(id);
    return parseServerResponse(response);
}

async function remove(id) {
    let response = await serverApi.remove(id);
    return parseServerResponse(response);
}

export {all, one, remove};

function parseServerResponse(response) {
    try {
        let info = JSON.parse(response);
        if (info.code === 200) {
            return info.data;
        } else {
            return info.status;
        }
    }
    catch (error) {
        throw new Error('incorrect answer');
    }
}
