function consolePrint(array, arg) {
    if (arg === undefined) {
        arg = array.length - 1;
    }
    if (arg > 0) {
        consolePrint(array, arg - 1);
    }
    console.log(array[arg]);
}

module.exports = consolePrint;

